using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif


public class AISensor : MonoBehaviour
{
    public float visionAngle = 45f;
    public float MaxVisionDistance = 20f;

    public Color VisionColor;
    public LayerMask mask;
    
    [Serializable]
    public class OnPLayerDetectClass : UnityEvent { }

    [FormerlySerializedAs("OnDetectPlayer")]
    [SerializeField]
    public OnPLayerDetectClass m_OnDetectPLayer = new OnPLayerDetectClass(); 
    
    [Serializable]
    public class OnPLayerLostClass : UnityEvent { }

    [FormerlySerializedAs("OnPlayerLost")]
    [SerializeField]
    public OnPLayerLostClass m_OnLostPLayer = new OnPLayerLostClass(); 
   

    private void Update()
    {
        Vector3 targetDirection = AIManager.instance.player.position - transform.position;
        float Angle = Vector3.Angle (targetDirection, transform.forward * MaxVisionDistance);
        
        if (Angle < visionAngle)
        {
            RaycastHit hit;

            if (Physics.Raycast(transform.position, targetDirection, out hit, MaxVisionDistance, mask))
            {
                if (hit.collider != null)
                {
                    if (hit.collider.transform == AIManager.instance.player)
                    {
                        //print("PlayerDetectado");

                        Debug.DrawRay(transform.position, targetDirection, Color.red);
                        m_OnDetectPLayer?.Invoke();
                    }
                    else
                    {
                        m_OnLostPLayer?.Invoke();
                    }
                }
            }

        }
        else
        {
            m_OnLostPLayer?.Invoke();
        }
    }
}

#if UNITY_EDITOR
[ExecuteAlways]
[CustomEditor(typeof(AISensor))]
public class EnemyVisionSensor : Editor
{
    private void OnSceneGUI()
    {
        var ai = target as AISensor;
        Vector3 startPoint = Mathf.Cos(-ai.visionAngle * Mathf.Deg2Rad) * ai.transform.forward +
                                        Mathf.Sin(ai.visionAngle * Mathf.Deg2Rad) * -ai.transform.right;

        Handles.color = ai.VisionColor;
        Handles.DrawSolidArc(ai.transform.position, Vector3.up, startPoint, ai.visionAngle * 2f, ai.MaxVisionDistance);
    }
}

#endif
